package com.example.aplikasisqllite;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.content.Intent;


public class Splash extends Activity {
    private int waktu_loading=10000;
    //4000=4detik

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent login = new Intent(Splash.this, Login.class);
                startActivity(login);
                finish();
            }
        }, waktu_loading);
    }
}
